#!/usr/bin/python3
# See also http://wiki.xkcd.com/geohashing/
# and of course http://www.xkcd.com/426/

import datetime
import imp
import importlib
import io
import json
import sys
import urllib.request
import webbrowser

ag_src = importlib.find_loader('antigravity').get_source('antigravity')
codestr = '\n'.join([x for x in ag_src.split('\n') if 'webbrowser' not in x])

munroe = imp.new_module('munroe')
exec(codestr, munroe.__dict__, munroe.__dict__)

OSM_URL_STR = 'https://www.openstreetmap.org/?mlat={}&mlon={}&zoom=16'
# OSM_URL_STR = 'https://openstreetmap.cz/?mlat={0}&mlon={1}&zoom=16&layers=kH#map=16/{0}/{1}layers=kH'
MAPY_URL_STR = 'https://mapy.cz/turisticka?' + \
    'x={1}&y={0}&z=16&source=coor&id={1}%2C{0}'
DJI_URL_STR = 'http://finance.google.com/finance/info?q=INDEXDJX:.DJI'

def ghash(lat, lon, last_dji):
    today = datetime.date.today()
    daybytes = today.isoformat().encode()

    oldstdout = sys.stdout
    sys.stdout = io.StringIO()
    munroe.geohash(lat, lon, daybytes + b'-' + last_dji.encode())
    value = sys.stdout.getvalue()
    sys.stdout.close()
    sys.stdout = oldstdout
    return [float(x) for x in value.split()]


def get_my_ip():
    req = urllib.request.Request(
        'https://ifconfig.co',
        headers={'Accept': 'application/json'})
    with urllib.request.urlopen(req) as ret:
        ret_str = json.loads(ret.read().decode())
        return ret_str['ip']


def get_my_location(ip):
    req = urllib.request.Request(
        'https://geoip.cf/api/{}'.format(ip),
        headers={'Accept': 'application/json'})
    with urllib.request.urlopen(req) as ret:
        ret_dict = json.loads(ret.read().decode())
        return ret_dict['latitude'], ret_dict['longitude']

def get_last_dji():
    with urllib.request.urlopen(DJI_URL_STR) as ret:
        ret_str = ret.read().strip()
        return json.loads(ret_str[3:].decode())[0]['pcls_fix']

my_location = get_my_location(get_my_ip())
where_to = ghash(my_location[0], my_location[1], get_last_dji())
webbrowser.open(OSM_URL_STR.format(*where_to))
